# JobJogger Registration Service
This repository contains the code of our JobJogger Registration Service. 
It handles loading, building, registering and deleting Dockerimages from a given Registry using a Docker child process.

## Environment Variables
| Name          | Description                                    |
|---------------|------------------------------------------------|
| PORT          | Port this Service should listen on             |
| DOCKER_HOST   | Address of the Docker daemon                   |
| REGISTRY_HOST | Hostname of the Docker registry                |
| DOCKER_USER   | Username for logging in to the Docker registry |
| DOCKER_PWD    | Password for logging in to the Docker registry |
