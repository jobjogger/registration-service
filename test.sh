docker pull hello-world
docker pull busybox
docker pull docker
docker save hello-world busybox docker | curl -H "Content-Type: archive/tar" --data-binary @- http://localhost:3000/vali
curl -X DELETE localhost:3000/vali/busybox:latest
curl -X DELETE localhost:3000/vali/hello-world:latest
curl -X DELETE localhost:3000/vali/docker:latest