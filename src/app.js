const express = require("express");
const logger = require("./helpers/logger");
const pruneImages = require("./helpers/pruneImages");
const buildAndPushImage = require("./services/buildAndPushImage");
const loadAndPushImages = require("./services/loadAndPushImages");
const deleteImage = require("./services/deleteImage");
const Image = require("./objects/Image");
const vars = require("./helpers/vars");
const childProcess = require("./helpers/childProcess");

const app = express();
const PORT = process.env.PORT || 3000;

const DOCKER_USER = process.env.DOCKER_USER || null;
const DOCKER_PWD = process.env.DOCKER_PWD || null;

if (DOCKER_USER && DOCKER_PWD) {
    childProcess("docker", ["login", vars.REGISTRY_HOST, "--username", DOCKER_USER, "--password", DOCKER_PWD], true)
}

app.post("/:user", function(req, res) {
    logger.info(
        "Received Request: " +
            JSON.stringify({
                ip: req.ip,
                method: req.method,
                baseUrl: req.baseUrl,
                params: req.params
            })
    );
    loadAndPushImages(req.params.user, req)
        .then(result => {
            logger.info(
                "Responded with: " + JSON.stringify(result)
            );
            res.set("Content-Type", "application/json");
            res.status(200).send(result);
            pruneImages();
        })
        .catch(err => {
            logger.error(JSON.stringify(err));
            res.status(err.status).send(err.msg);
            pruneImages();
        });

});

app.delete("*", (req, res) => {
    let imageId = req.path.substr(1);
    logger.info("Received request to delete " + imageId);

    // get repository and tag from imageId;
    if (imageId.includes(":")) {
        var name = imageId.substr(0, imageId.indexOf(":"));
        var tag = imageId.substr(imageId.indexOf(":") + 1);
    } else {
        var name = imageId
        var tag = "latest";
    }


    deleteImage(name, tag)
        .then(result => {
            res.status(200).send(result);
        })
        .catch(err => {
            logger.error(JSON.stringify(err));
            res.status(err.status).send(err.msg);
        });
});

app.post("/build/:user/:name", function(req, res) {
    logger.info(
        "Received Request to build image " +
            req.params.name +
            " for user " +
            req.params.user
    );
    buildAndPushImage(req.params.user, req.params.name, req)
        .then(result => {
            logger.info("responded with: " + JSON.stringify(result));
            res.status(200).send(result);
            pruneImages();
        })
        .catch(err => {
            let image = new Image(req.params.name+":latest");
            image.tag(req.params.user, vars.REGISTRY_HOST + "/" + user + "/" + image.image);
            image.fail(err.msg);
            logger.error(JSON.stringify(err));
            res.status(200).send(image);
            pruneImages();
        });
});

app.listen(PORT);
logger.info("registration-service listening on port " + PORT);
