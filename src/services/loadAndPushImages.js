const logger = require("../helpers/logger");
const loadImages = require("./loadImages");
const tagImages = require("./tagImages");
const pushImages = require("./pushImages");
const Error = require("../objects/Error");

module.exports = function(user, stream) {
    return new Promise((resolve, reject) => {
        loadImages(stream)
            .then(images => {
                tagImages(images, user)
                    .then(tags => {
                        pushImages(tags.success, tags.fail)
                            .then(result => {
                                resolve(result);
                            })
                            .catch(result => {
                                resolve(result);
                            });
                    })
                    .catch(tags => {
                        if (tags.success.length == 0) {
                            logger.sub.error("failed to tag all images");
                            resolve(tags);
                        } else {
                            logger.sub.error("failed to tag some images");
                            pushImages(tags.success, tags.fail)
                                .then(result => {
                                    resolve(result);
                                })
                                .catch(result => {
                                    resolve(result);
                                });
                        }
                    });
            })
            .catch(error => {
                reject(new Error(error, 500));
            });
    });
};
