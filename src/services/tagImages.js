const childProcess = require("../helpers/childProcess");
const logger = require("../helpers/logger");
const REGISTRY_HOST = require("../helpers/vars").REGISTRY_HOST;

module.exports = function(images, user) {
    return new Promise((resolve, reject) => {
        logger.info("Tagging Images" + images.toString() + "...");
        let result = {
            success: [],
            fail: []
        };
        for (let i = 0; i < images.length; i++) {
            // Start process for each image
            let image = images[i];
            let uri = REGISTRY_HOST + "/" + user + "/" + image.image;
            logger.sub.info("Tagging image " + image + " with " + uri);
            childProcess("docker", ["tag", image.image, uri])
                .then(() => {
                    image.tag(user, uri);
                    image.inspect(function() {
                        result.success.push(image);
                        logger.sub.info(
                            "Tagged " + image.image + " with " + uri
                        );
                        if (result.success.length == images.length) {
                            resolve(result);
                        } else if (
                            result.fail.length + result.success.length ==
                            images.length
                        ) {
                            reject(result);
                        }
                    });
                })
                .catch(error => {
                    logger.sub.error(
                        "Could not tag " + image.image + " with " + uri
                    );
                    image.fail(
                        "Could not tag " +
                            image.image +
                            " with " +
                            uri +
                            " (" +
                            JSON.stringify(error) +
                            ")"
                    );
                    result.fail.push(image);
                    if (
                        result.fail.length + result.success.length ==
                        images.length
                    ) {
                        reject(result);
                    }
                });
        }
    });
};
