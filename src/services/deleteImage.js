var http = require("http");
if (process.env.HTTPS_REGISTRY == "true") {
    http = require("https");
}
const logger = require("../helpers/logger");
const REGISTRY_HOSTNAME = require("../helpers/vars").REGISTRY_HOSTNAME;
const REGISTRY_PORT = require("../helpers/vars").REGISTRY_PORT;
const Error = require("../objects/Error");
const Buffer = require("buffer").Buffer;

module.exports = function(name, tag) {
    let imageId = name + ":" + tag;
    return new Promise((resolve, reject) => {
        const deleteFunction = function(auth) {
            // api call to get digest
            logger.sub.info("Obtaining digest...");
            let digestPath = "/v2/" + name + "/manifests/" + tag;
            let digestOptions  = {
                host: REGISTRY_HOSTNAME,
                path: digestPath,
                method: "HEAD",
                headers: {
                    Accept:
                        "application/vnd.docker.distribution.manifest.v2+json"
                }
            };
            if (REGISTRY_PORT) digestOptions.port = REGISTRY_PORT
            if(auth) {
                digestOptions.headers.Authorization = auth;
            }
            let digestReq = http.request(
                digestOptions,
                innerRes => {
                    if (innerRes.statusCode === 200) {
                        let digest = innerRes.headers["docker-content-digest"];
                        logger.sub.info("Deleting Image with digest: " + digest);
                        //api call to delete digest
                        let deletePath = "/v2/" + name + "/manifests/" + digest;
                        let deleteOptions = {
                            host: REGISTRY_HOSTNAME,
                            path: deletePath,
                            method: "DELETE",
                            headers: {}
                        }
                        if (REGISTRY_PORT) deleteOptions.port = REGISTRY_PORT
                        if(auth) deleteOptions.headers.Authorization = auth;
                        let deleteReq = http.request(
                            deleteOptions,
                            innerRes => {
                                if (innerRes.statusCode === 202) {
                                    logger.sub.info(
                                        "Successfully deleted " + imageId
                                    );
                                    resolve(imageId + " deleted successfully.");
                                } else {
                                    logger.sub.error(
                                        "Error while deleting " +
                                        imageId +
                                        ", registry returned " +
                                        innerRes.statusCode
                                    );
                                    reject(
                                        new Error(
                                            "There was an error while deleting " +
                                            imageId +
                                            ", registry returned " +
                                            innerRes.statusCode
                                        )
                                    );
                                }
                            }
                        );
                        deleteReq.end();
                    } else if (innerRes.statusCode === 404) {
                        logger.sub.info("Image " + imageId + " not found.");
                        reject(new Error(imageId + " not found.", 404));
                    } else if (innerRes.statusCode == 401 && !auth) {
                        logger.sub.info("Authentication needed");
                        if (process.env.DOCKER_USER && process.env.DOCKER_PWD) {
                            let url = "https://jobjogger.azurecr.io/oauth2/token?service=" + REGISTRY_HOSTNAME + "&scope=repository:" + name + ":*";
                            logger.sub.info("Obtaining Authentication from " + url);
                            let accessTokenReq = http.get(url, {headers: {Authorization: "Basic " + Buffer.from(process.env.DOCKER_USER + ":" + process.env.DOCKER_PWD).toString('base64')}}, innerRes => {
                                let token = "";
                                innerRes.on('data', (d) => {
                                    token += d;
                                });
                                innerRes.on("end", () => {
                                    if(innerRes.statusCode == 200) {
                                        token = JSON.parse(token)["access_token"];
                                        deleteFunction("Bearer "+token);
                                    } else {
                                        reject("Something went wrong while obtaining the access token");
                                    }
                                })

                            });
                        } else {
                            reject("Authentication needed for "+ REGISTRY_HOSTNAME)
                        }
                    } else {
                        logger.sub.error(
                            "Error while obtaining digest, registry responded with: " +
                            innerRes.statusCode
                        );
                        reject(
                            new Error(
                                "Error while obtaining digest, registry responded with: " +
                                innerRes.statusCode
                            )
                        );
                    }
                }
            );
            digestReq.end();
        }
        deleteFunction();
    });
};
