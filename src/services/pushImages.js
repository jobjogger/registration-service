const childProcess = require("../helpers/childProcess");
const logger = require("../helpers/logger");

module.exports = function(images, failed) {
    return new Promise((resolve, reject) => {
        logger.info("Pushing Images...")
        if (failed == undefined) failed = [];
        let result = {
            success: [],
            fail: failed
        };
        for (let i = 0; i < images.length; i++) {
            // Start process for each image
            let image = images[i];
            logger.sub.info("Pushing " + image.uri + "...");
            childProcess("docker", ["push", image.uri])
                .then(output => {
                    logger.sub.info("Successfully pushed " + image.uri + "!");
                    result.success.push(image);
                    if (result.success.length == images.length) {
                        resolve(result);
                    } else if (
                        result.fail.length + result.success.length ==
                        images.length
                    ) {
                        reject(result);
                    }
                })
                .catch(error => {
                    logger.sub.error(
                        "Could not push " + image.uri + " " + error
                    );
                    image.fail("Could not push " + image.uri + " " + error);
                    result.fail.push(image);
                    if (
                        result.fail.length + result.success.length ==
                        images.length
                    ) {
                        reject(result);
                    }
                });
        }
    });
};
