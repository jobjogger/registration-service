const childProcess = require("../helpers/childProcess");
const Image = require("../objects/Image");
const logger = require("../helpers/logger");

module.exports = function(stream) {
    return new Promise((resolve, reject) => {
        logger.info("Loading Images...");
        // Spawn child process for docker load
        childProcess("docker", ["load"], true, stream)
            .then(output => {
                output = output.match(/Loaded image: (.*)\n/g);
                let images = [];
                for (let i = 0; i < output.length; i++) {
                    let image = new Image(output[i].match(/[^\s]*(?=\n)/)[0]);
                    images.push(image);
                }
                logger.sub.info("Loaded images: " + images.toString());
                resolve(images);
            })
            .catch(error => {
                reject(error);
            });
    });
};
