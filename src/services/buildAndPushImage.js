const childProcess = require("../helpers/childProcess");
const tagImages = require("./tagImages");
const pushImages = require("./pushImages");
const Image = require("../objects/Image");
const Error = require("../objects/Error");

module.exports = function(user, name, stream) {
    return new Promise((resolve, reject) => {
        childProcess("docker", ["build", "-t", name, "-"], true, stream)
            .then(() => {
                let images = [new Image(name)];
                tagImages(images, user)
                    .then(tags => {
                        pushImages(tags.success, tags.fail)
                            .then(result => {
                                resolve(result);
                            })
                            .catch(err => {
                                reject(new Error(err, 500));
                            });
                    })
                    .catch(err => {
                        reject(new Error(err, 500));
                    });
            })
            .catch(err => {
                reject(new Error(err, 500));
            });
    });
};
