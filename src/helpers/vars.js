const REGISTRY_HOST = process.env.REGISTRY_HOST || "localhost:5000";
const PORT = process.env.PORT || 3000;
const REGISTRY_HOSTNAME = REGISTRY_HOST.split(":")[0];
const REGISTRY_PORT = REGISTRY_HOST.split(":")[1];

module.exports = {
    REGISTRY_HOST: REGISTRY_HOST,
    PORT: PORT,
    REGISTRY_HOSTNAME: REGISTRY_HOSTNAME,
    REGISTRY_PORT: REGISTRY_PORT
};
