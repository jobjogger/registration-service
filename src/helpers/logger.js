module.exports = {
    info: function(msg) {
        console.log(msg);
    },
    error: function(msg) {
        console.error("Error: " + msg);
    },
    sub: {
        info: function(msg) {
            console.log("  > " + msg);
        },
        error: function(msg) {
            console.error("  > Error: " + msg);
        }
    }
};
