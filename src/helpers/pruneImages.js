const childProcess = require("./childProcess");

module.exports = function() {
    childProcess("docker", ["image", "prune", "-a", "-f"])
        .then(result => {
            console.log(result);
        })
        .catch(err => {
            console.log(err);
        });
};
