const { spawn } = require("child_process");
const logger = require("./logger");

function log(cmd, msg) {
    process.stdout.write(`[${cmd}] - ${msg}`);
}

module.exports = function(cmd, args, report, stdin) {
    return new Promise((resolve, reject) => {
        // Spawn process.
        let process = spawn(cmd, args);

        // Handle process stdin.
        if (stdin) {
            stdin.pipe(process.stdin);
            stdin.on("end", () => {
                process.stdin.end();
            });
        }

        // Get ready to save output.
        let output = "";

        // Handle process stdout.
        process.stdout.on("data", data => {
            //console.log(`${cmd} ${args.toString()}: ${data}`);
            if (data != "") {
                output += data;
                if (report == true || report == undefined) log(cmd, data);
            }
        });

        // Handle process stderror.
        process.stderr.on("data", data => {
            //console.error(`${cmd} ${args.toString()}: error: ${data}`);
            if (data != "") {
                output += data;
                if (report == true || report == undefined) log(cmd, data);
            }
        });

        // Handle process exit
        process.on("close", code => {
            if (code == 0) {
                resolve(output);
            } else {
                reject(output);
            }
        });
    });
};
