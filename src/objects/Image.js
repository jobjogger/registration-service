const childProcess = require("../helpers/childProcess");

module.exports = class {
    constructor(image) {
        this.image = image;
    }

    fail(reason) {
        this.reason = reason;
        this.failed = true;
    }

    tag(username, uri) {
        this.user = username;
        this.image = username + "/" + this.image;
        this.uri = uri;
    }

    inspect(cb) {
        if (this.uri !== undefined) {
            childProcess("docker", ["inspect", this.uri], false)
                .then(meta => {
                    let inspect = JSON.parse(meta)[0];
                    this.meta = {
                        created: inspect["Created"],
                        comment: inspect["Comment"],
                        env: inspect["ContainerConfig"]["Env"],
                        cmd: inspect["ContainerConfig"]["Cmd"],
                        workdir: inspect["ContainerConfig"]["WorkingDir"],
                        entrypoint: inspect["ContainerConfig"]["Entrypoint"],
                        labels: inspect["ContainerConfig"]["Labels"],
                        dockerversion: inspect["DockerVersion"],
                        author: inspect["Author"],
                        architecture: inspect["Architecture"],
                        os: inspect["OS"],
                        size: inspect["Size"],
                        virtualsize: inspect["VirtualSize"]
                    };
                    cb();
                })
                .catch(err => {
                    console.log(
                        "  > Error while getting info for image " +
                            this.image +
                            ": " +
                            err
                    );
                    cb();
                });
        } else {
            console.log(
                "  > Error while getting info for image " +
                    this.image +
                    ": Image has not been tagged yet"
            );
            cb();
        }
    }

    toString() {
        return JSON.stringify(this);
    }
};
