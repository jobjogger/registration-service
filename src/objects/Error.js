module.exports = class Error {
    constructor(msg, status) {
        this.msg = msg;
        if (status) this.status = status;
    }
}