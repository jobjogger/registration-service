FROM docker:latest
RUN apk add --update nodejs npm && npm install yarn -g
COPY package.json ./
COPY yarn.lock ./
RUN yarn install --production
COPY src ./
EXPOSE 3000
ENTRYPOINT ["node", "app.js"]